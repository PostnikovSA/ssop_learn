#pragma once

struct tNode
{
	int value;
	struct tNode* next;
};

int getN();

tNode* create_list(int N);

void print_list(tNode* p_begin);

void delete_list(tNode* p_begin);

void print_sum_arithmetic_mean_of_couples(tNode* p_begin);