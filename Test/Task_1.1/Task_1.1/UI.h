
#pragma once
#include "NumbersData.h"
#include <iostream>
using namespace std;

void PrintAllNumbers()
{
	for (int i = GetMinNum(); i <= GetMaxNum(); i += GetStep())
	{
		cout << i << " ";
	}
	cout << "\n";
}

void PrintMultiples() 
{
	for (int i = GetMinNum(); i <= GetMaxNum(); i += GetStep())
	{
		if ((i % GetMultiple()) == 0)
			cout << i << " ";
	}
	cout << "\n";
}

void PrintSumMultiplesWithOperatorFor() 
{
	cout << "Sum of all multiples with operator FOR: " << GetSumMultipleWithOperatorFor() << "\n\n";
}

void PrintSumMultiplesWithOperatorWhile()
{
	cout << "Sum of all multiples with operator WHILE: " << GetSumMultipleWithOperatorWhile() << "\n\n";
}

void PrintSumMultiplesWithOperatorDoWhile()
{
	cout << "Sum of all multiples with operator DOWHILE: " << GetSumMultipleWithOperatorDoWhile() << "\n\n";
}

void RunMenu()
{
	bool isExit = false;

	while (isExit == false)
	{
		cout << "Menu" << "\n\n";
		cout << "[1] Print all numbers" << "\n";
		cout << "[2] Print multiples" << "\n";
		cout << "[3] Print Sum of all multiples with operator FOR" << "\n";
		cout << "[4] Print Sum of all multiples with operator WHILE" << "\n";
		cout << "[5] Print Sum of all multiples with operator DOWHILE" << "\n";
		cout << "[6] Exit" << "\n\n";

		int numMenu = 0;
		while (!(cin >> numMenu) || (cin.peek() != '\n'))
		{
			cout << "Input only number from 1 to 6: ";
			cin.clear();
			while (cin.get() != '\n');
			break;
		}

		switch (numMenu)
		{
		case 1:
			system("cls");
			cout << "Print all numbers" << "\n\n";
			PrintAllNumbers();
			system("pause");
			system("cls");
			break;
		case 2:
			system("cls");
			cout << "Run Print multiples" << "\n\n";
			PrintMultiples();
			system("pause");
			system("cls");
			break;
		case 3:
			system("cls");
			cout << "Run Print Sum of all multiples with operator FOR" << "\n\n";
			PrintSumMultiplesWithOperatorFor();
			system("pause");
			system("cls");
			break;
		case 4:
			system("cls");
			cout << "Run Print Sum of all multiples with operator WHILE" << "\n\n";
			PrintSumMultiplesWithOperatorWhile();
			system("pause");
			system("cls");
			break;
		case 5:
			system("cls");
			cout << "Run Print Sum of all multiples with operator DOWHILE" << "\n\n";
			PrintSumMultiplesWithOperatorDoWhile();
			system("pause");
			system("cls");
			break;
		case 6:
			system("cls");
			cout << "Run Exit" << "\n\n";
			isExit = true;
			system("pause");
			system("cls");
			break;
		default:
			system("cls");
			cout << "Error. Try again. Input only number from 1 to 6" << "\n\n";
			system("pause");
			system("cls");
			break;
		}
	}
}
