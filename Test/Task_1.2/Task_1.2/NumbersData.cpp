
#include <iostream>
using namespace std;

double n = 0;
double k = 1;

double getN()
{
	n = 0;
	while (true)
	{
		system("cls");
		cout << "Input only number from -20.0 to 20.0: ";

		while (!(cin >> n) || (cin.peek() != '\n'))
		{
			cin.clear();
			n = 100; // �������
			while (cin.get() != '\n');
			break;
		}
		if ((n >= -20.0) && (n <= 20.0))
			break;
	}
	cout << n << "\n";
	return n;
}
double getK()
{
	return k;
}

double PrintValueFunctionWithOperatorFor()
{
    double result = 0;

    for (double count = n; count < k; count++)
        result += (pow((double)(- 1), k + (double)1)) / (k * (k + (double)1));

    return result;
}

double PrintValueFunctionWithOperatorWhile()
{
	double result = 0;
	double count = n;

	while (count < k)
	{
		result += (pow((double)(-1), k + (double)1)) / (k * (k + (double)1));
		count++;
	}

	return result;
}

double PrintValueFunctionWithOperatorDoWhile()
{
	double result = 0;
	double count = n;

	do
	{
		result += (pow((double)(-1), k + (double)1)) / (k * (k + (double)1));
		count++;
	} while (count < k);


	return result;
}
