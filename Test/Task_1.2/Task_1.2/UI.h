
#pragma once
#include "NumbersData.h"
#include <iostream>
using namespace std;

void RunMenu()
{
	bool isExit = false;

	while (isExit == false)
	{
		cout << "Menu" << "\n\n";
		cout << "[1] Input n" << "\n";
		cout << "[2] Print k" << "\n";
		cout << "[3] Print value function with operator FOR" << "\n";
		cout << "[4] Print value function with operator WHILE" << "\n";
		cout << "[5] Print value function with operator DOWHILE" << "\n";
		cout << "[6] Exit" << "\n\n";

		int numMenu = 0;
		while (!(cin >> numMenu) || (cin.peek() != '\n'))
		{
			cout << "Input only number from 1 to 6: ";
			cin.clear();
			while (cin.get() != '\n');
			break;
		}

		switch (numMenu)
		{
		case 1:
			system("cls");
			cout << "Run Print all numbers" << "\n\n";
			getN();
			system("pause");
			system("cls");
			break;
		case 2:
			system("cls");
			cout << "Run Print step" << "\n\n";
			cout << getK() << "\n\n";
			system("pause");
			system("cls");
			break;
		case 3:
			system("cls");
			cout << "Run Print value function with operator FOR" << "\n\n";
			cout << PrintValueFunctionWithOperatorFor() << "\n";
			system("pause");
			system("cls");
			break;
		case 4:
			system("cls");
			cout << "Run Print value function with operator WHILE" << "\n\n";
			cout << PrintValueFunctionWithOperatorWhile() << "\n";
			system("pause");
			system("cls");
			break;
		case 5:
			system("cls");
			cout << "Run Print value function with operator DOWHILE" << "\n\n";
			cout << PrintValueFunctionWithOperatorDoWhile() << "\n";
			system("pause");
			system("cls");
			break;
		case 6:
			system("cls");
			cout << "Run Exit" << "\n\n";
			isExit = true;
			system("pause");
			system("cls");
			break;
		default:
			system("cls");
			cout << "Error. Try again. Input only number from 1 to 6" << "\n\n";
			system("pause");
			system("cls");
			break;
		}
	}
}


