
#pragma once

double GetMinNum();

double GetMaxNum();

double GetStep();

double GetValue(double num);
