
#pragma once
#include "NumbersData.h"
#include <iostream>
using namespace std;

void PrintAllNumbers()
{
	for (double count = GetMinNum(); count < (GetMaxNum() + GetStep()); count += GetStep())
	{
		if ((count >= -0.1) && (count <= 0.1)) 
		{
			cout << "0" << "\n";
		}
			
		else
		{
			cout << count << "\n";
		}
	}
	cout << "\n\n";
}

void PrintStep()
{
	cout << "Step: " << GetStep() << "\n\n";
}

void PrintValueFunctionWithOperatorFor() 
{
	for (double count = GetMinNum(); count <= GetMaxNum(); count += GetStep())
	{
		if ((count > -0.1) && (count < 0.1)) 
		{
			cout << "Error! division by 0\n";
			continue;
		}

		if ((count > -2.1) && (count < -1.9)) 
		{
			cout << "Error! division by 0\n";
			continue;
		}

		cout << GetValue(count) << "\n";
	} 
	cout << "\n";
}

void PrintValueFunctionWithOperatorWhile()
{
	double count = GetMinNum();
	while (count < GetMaxNum())
	{
		if ((count > -0.1) && (count < 0.1))
		{
			cout << "Error! division by 0\n";
		}
		else
		if ((count > -2.1) && (count < -1.9))
		{
			cout << "Error! division by 0\n";
		}
		else
		cout << GetValue(count) << "\n";

		count += GetStep();
	}
	cout << "\n";
}

void PrintValueFunctionWithOperatorDoWhile()
{
	double count = GetMinNum();
	do
	{
		if ((count > -0.1) && (count < 0.1))
		{
			cout << "Error! division by 0\n";
		}
		else
		if ((count > -2.1) && (count < -1.9))
		{
			cout << "Error! division by 0\n";
		}
		else
		cout << GetValue(count) << "\n";

		count += GetStep();
	} 
	while (count < GetMaxNum());
	cout << "\n";
}

void RunMenu()
{
	bool isExit = false;

	while (isExit == false)
	{
		cout << "Menu" << "\n\n";
		cout << "[1] Print all numbers" << "\n";
		cout << "[2] Print step" << "\n";
		cout << "[3] Print value function with operator FOR" << "\n";
		cout << "[4] Print value function with operator WHILE" << "\n";
		cout << "[5] Print value function with operator DOWHILE" << "\n";
		cout << "[6] Exit" << "\n\n";

		int numMenu = 0;
		while (!(cin >> numMenu) || (cin.peek() != '\n'))
		{
			cout << "Input only number from 1 to 6: ";
			cin.clear();
			while (cin.get() != '\n');
			break;
		}

		switch (numMenu)
		{
		case 1:
			system("cls");
			cout << "Run Print all numbers" << "\n\n";
			PrintAllNumbers();
			system("pause");
			system("cls");
			break;
		case 2:
			system("cls");
			cout << "Run Print step" << "\n\n";
			PrintStep();
			system("pause");
			system("cls");
			break;
		case 3:
			system("cls");
			cout << "Run Print value function with operator FOR" << "\n\n";
			PrintValueFunctionWithOperatorFor();
			system("pause");
			system("cls");
			break;
		case 4:
			system("cls");
			cout << "Run Print value function with operator WHILE" << "\n\n";
			PrintValueFunctionWithOperatorWhile();
			system("pause");
			system("cls");
			break;
		case 5:
			system("cls");
			cout << "Run Print value function with operator DOWHILE" << "\n\n";
			PrintValueFunctionWithOperatorDoWhile();
			system("pause");
			system("cls");
			break;
		case 6:
			system("cls");
			cout << "Run Exit" << "\n\n";
			isExit = true;
			system("pause");
			system("cls");
			break;
		default:
			system("cls");
			cout << "Error. Try again. Input only number from 1 to 6" << "\n\n";
			system("pause");
			system("cls");
			break;
		}
	}
}

