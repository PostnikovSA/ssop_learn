#pragma once

#include "TData.h"
#include <iostream>
using namespace std;

int** Create(int n, int m)
{
    int** numArray = new int* [n];
    for (int i = 0; i < n; i++)
    {
        numArray[i] = new int[m];
    }
    return numArray;
}

void Print(int** M, int n, int m)
{
    for (int i = 0; i < n; i++)
    {
        for (int j = 0; j < m; j++)
        {
            cout << M[i][j] << "\t";
        }
        cout << "\n\n";
    }
}

void StartCreateArray() 
{
	int n = getN();
	int m = getM();

	int** numArray = Create(n, m);

	cout << "\nArray created\n\n";
}

void Interface()
{
	system("cls");

	bool isExit = false;

	int n = getN();
	int m = getM();
	int** numArray = Create(n, m);
	bool isArrayExist = true;

	while (isExit == false)
	{
		cout << "Menu" << "\n\n";
		cout << "[1] Create array" << "\n";
		cout << "[2] Input in array" << "\n";
		cout << "[3] Print Matrix" << "\n";
		cout << "[4] Average for columns M" << "\n";
		cout << "[5] Delete array" << "\n";
		cout << "[6] Exit" << "\n\n";

		int numMenu = 0;
		while (!(cin >> numMenu) || (cin.peek() != '\n'))
		{
			cout << "Input only number from 1 to 6: ";
			cin.clear();
			while (cin.get() != '\n');
			break;
		}

		switch (numMenu)
		{
		case 1:
			system("cls");
			cout << "Run StartCreateArray" << "\n\n";
			if (isArrayExist == true)
				cout << "Array already exists\n\n";
			else 
			{
				StartCreateArray();
				isArrayExist = true;
			}
			system("pause");
			system("cls");
			break;

		case 2:
			system("cls");
			cout << "Run InputInArray" << "\n\n";
			if (isArrayExist == false)
				cout << "Array no exists\n\n";
			else
				Input(numArray, n, m);
			cout << "\n";
			system("pause");
			system("cls");
			break;

		case 3:
			system("cls");
			cout << "Run Print" << "\n\n";
			if (isArrayExist == false)
				cout << "Array no exists\n\n";
			else
				Print(numArray, n, m);
			system("pause");
			system("cls");
			break;

		case 4:
			system("cls");
			cout << "Run Average for columns" << "\n\n";
			if (isArrayExist == false)
				cout << "Array no exists\n\n";
			else
				Change(numArray, n, m);
			system("pause");
			system("cls");
			break;

		case 5:
			system("cls");
			cout << "Run Delete array" << "\n\n";
			if (isArrayExist == true)
			{
				Del(numArray, n, m);
				isArrayExist = false;
			}
			else
				cout << "Array no exists\n\n";
			system("pause");
			system("cls");
			break;
		case 6:
			system("cls");
			cout << "Run Exit" << "\n\n";
			isExit = true;
			system("pause");
			system("cls");
			break;

		default:
			system("cls");
			cout << "Error. Try again. Input only number from 1 to 6" << "\n\n";
			system("pause");
			system("cls");
			break;
		}
	}
}
