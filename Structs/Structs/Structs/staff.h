using namespace std;

const int sizeStaff = 3;
TWorker staff[sizeStaff];

int countStaff = 0;

int GetIndexWorkerWithMaxSalary(TWorker staff[], int countStaff)
{
	float maxSalary = staff[0].salary;
	int index = 0;

	for (int i = 1; i < countStaff; i++)
	{
		if (maxSalary < staff[i].salary)
		{
			maxSalary = staff[i].salary;
			index = i;
		}
	}

	return index;
}

int GetCountWorkersFullTime(TWorker staff[], int countStaff)
{
	int countWorkersFullTime = 0;
	for (int i = 0; i < countStaff; i++)
	{
		if (staff[i].code == 1)
			countWorkersFullTime++;
	}
	return countWorkersFullTime;
}

int GetIndexWorkerFullTime(TWorker staff[], int countStaff)
{
	int index = 0;
	for (int i = 0; i < countStaff; i++)
	{
		if (staff[i].code == 1)
			index++;
	}
	return index;
}