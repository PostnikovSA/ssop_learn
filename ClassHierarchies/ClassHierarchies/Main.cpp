﻿
#include <iostream>
#include "TCats.h"
using namespace std;

int main()
{
    char species[5] = "Cats";
    char name[7] = "Pyshok";
    char gender[5] = "Male";
    char color[6] = "White";
    char breed[10] = "Persian";

    TCatsAnimals cats(species, name, gender, color, 3, breed);
    cats.parameters(30, 9);
}